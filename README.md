# Telegram Utils Lib

I created this library cause I find myself using these files in all my telegram bots


# Setup

Before using these library a few notes:

This library uses some environement variables to work properly so if you wanna use this you have to declare them; I will list all of the while describing the packages with the functions

# MongoDB

the file inside `telegram_utils/utils/database.py` has a function to connect to a mongodb database using mongoengine.

To work you need to define a few environement variables

    DBUSERNAME = The username to connect to mongodb
    DBPASSWORD = The password to connect to mongodb
    DBHOST = The hostname of the machine hosting the mongodb
    DBNAME = The database you want to connect to
    TOKEN = The telegram token
    TELEGRAM_BOT_ADMIN = The chat_id of the admin of the bot used to send some logging message

# Logger

There's a file under `telegram_utils/utils/logger.py` to automatically create rotating log files called `server.log` that will automatically be renamed to `server.log.1`, `server.log.2`, etc... and that will be deleted after the tenth is created

# Process

The file `telegram_utils/utils/process.py` will allow you to create scheduled operation that will run in the background with the ability to restart it and stop it; this is used later with the telegram api

To work you need to define the environement variable `PROCESS_PREFIX` wich will be used to retrieve the process you created using a key passed during it's creation

### Example

    PROCESS_PREFIX="custom_bot" 


# Telegram

Under `telegram_utils/utils/telegram.py` you can find a file with various functions to perform various actions using the telegram bot api

    - send_message: Send a message to a designated chat
    - notice_admin: Send a message specifically to the admin of the bot
    - send_and_delete: Send a message and then delete it automatically after a timeout (using The Process of before)
    - delete_if_private: Delete a message if has been sent over a private chat

# Installation

To install it just type

```bash
    python setup.py install --user
```
