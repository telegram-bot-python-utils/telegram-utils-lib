#!/usr/bin/env python3

from typing import List
from mongoengine.errors import DoesNotExist
from telegram import InlineKeyboardMarkup
from telegram_utils.model.traslation import Translation
import telegram_utils.helper.redis as redis
import telegram_utils.utils.logger as logger
from telegram_utils.model.button import Button
from telegram_utils.utils.tutils import create_button
from telegram_utils.model.traslation import Translation
from telegram_utils.model.translation_misc import TranslationMisc
from telegram_utils.constants.messages import TRANSLATION_PREFIX, TRANSLATION_TABLE_NAME, TRANSLATION_TABLE_NAME_MISC

def load_translations():
    """ load all translations from the database to redis """
    # extract all the translations from the database
    translations: List[Translation] = Translation.objects
    logger.info(f"found {len(translations)} translations in the database")
    for translation in translations:
        # creating a unique identified for the translation using also the prefix from the environment variable
        code = f"{TRANSLATION_PREFIX}.{translation.code}.{translation.language}"
        logger.info(f"saving translation with code {code} into redis")
        # save the data to redis
        redis.save(code, translation.message)

def retrieve_default_translation(code: str, language: str, default: str = "it"):
    """ retrieve a translation from redis using the code and the language from the user/group """
    # creating a unique identified for the translation using also the prefix from the environment variable
    logger.info(f"loading translation with code {code} for language {language}")
    try:
        # search for a translation with the code and language
        translation: TranslationMisc = TranslationMisc.objects.get(code=code, language=language)
        return translation.message
    except DoesNotExist:
        try:
            # search for a translation with the code and default language_code
            translation: TranslationMisc = TranslationMisc.objects.get(
                code=code, language=default
            )
            return translation.message
        except DoesNotExist:
            # return the code when a translation is not found
            logger.error("Unable to find a translation for the specified code")
            return f"{code}_{language}"

def retrieve_translation(code: str, language: str):
    """ retrieve a translation from redis using the code and the language from the user/group """
    # creating a unique identified for the translation using also the prefix from the environment variable
    translation_code = f"{TRANSLATION_PREFIX}.{code}.{language}"
    logger.info(f"loading translation with code {translation_code} from redis")
    # retrieve the translation from redis
    message: str = redis.retrieve(translation_code)
    # return the message back if found, otherwise return the code
    return message.decode() if message else translation_code

def retrieve_db_translation(code: str, language: str, default: str = "it"):
    """ retrieve a translation from redis using the code and the language from the user/group """
    # creating a unique identified for the translation using also the prefix from the environment variable
    logger.info(f"loading translation with code {code} for language {language}")
    try:
        # search for a translation with the code and language
        translation: Translation = Translation.objects.get(code=code, language=language)
        return translation.message
    except DoesNotExist:
        try:
            # search for a translation with the code and default language_code
            translation: Translation = Translation.objects.get(
                code=code, language=default
            )
            return translation.message
        except DoesNotExist:
            # return the code when a translation is not found
            logger.error("Unable to find a translation for the specified code")
            return f"{code}_{language}"


def build_keyboard(buttons: List[List[Button]], language: str):
    """ build a Telegram compatible keyboard using a List[List[Button]] """
    keyboard = []
    for line in buttons:
        keyboard_line = []
        for button in line:
            # retrieve the translation for the button text
            button.caption = retrieve_translation(button.code, language)
            # create a Telegram compatible version of the button
            button = create_button(button.caption, button.callback, button.link)
            # append the created button to the list
            keyboard_line.append(button)
        # add the list to the keyboard
        keyboard.append(keyboard_line)
    # return a Telegram compatible keyboard
    return InlineKeyboardMarkup(keyboard)


def get_differences(misc: bool = False):
    table = TRANSLATION_TABLE_NAME_MISC if misc else TRANSLATION_TABLE_NAME
    table = table.replace("-quality", "") if "-quality" in table else "%s-quality" % table
    query = [
        { "$project": { "code": 1, "message": 1, "language": 1, "_id": 0 } },
        { "$unionWith": { "coll": table, "pipeline": [ { "$project": { "code": 1,"message": 1, "language": 1, "_id": 0 } } ]} },
        { "$group": {
          "_id": { "code": "$code", "language": "$language", "message": "$message" },
          "count": { "$sum": 1 }
          }
        },
        {"$match": {"_id" :{ "$ne" : None } , "count" : {"$eq": 1} } },
        {
          "$count": 'count'
        }
    ]
    if not misc:
        cursor = Translation.objects.aggregate(*query)
    else:
        cursor = TranslationMisc.objects.aggregate(*query)
    total = 0
    for doc in cursor:
        total = doc["count"] // 2
    return total