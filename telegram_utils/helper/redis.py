#!/usr/bin/env python3
from redis import Redis

redis: Redis = Redis()


def retrieve(key: str):
    """ retrieve the value from redis using the key """
    return redis.get(key)


def save(key: str, value: str):
    """ store a pair of key value in redis """
    redis.set(key, value)
