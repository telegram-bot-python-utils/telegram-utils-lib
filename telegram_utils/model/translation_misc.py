#!/usr/bin/env python3

from telegram_utils.constants.messages import TRANSLATION_TABLE_NAME_MISC
from telegram_utils.model.base_model import BaseModel
from mongoengine import StringField


class TranslationMisc(BaseModel):
    """ representation of a message in a specific language """

    meta = {"db_alias": "translation", "collection": TRANSLATION_TABLE_NAME_MISC}

    code = StringField()
    message = StringField()
    language = StringField()