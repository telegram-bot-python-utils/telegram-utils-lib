#!/usr/bin/env python3


class Button:
    """ representation of a Telegram button """

    # fmt: off
    def __init__(self, caption: str = None, code: str = None, callback: str = None, link: str = None):
    # fmt: on
        self.caption: str = caption
        self.code: str = code
        self.link: str = link
        self.callback: str = callback
