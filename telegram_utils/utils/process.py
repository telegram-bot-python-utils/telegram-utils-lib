#!/usr/bin/env python3

""" file to handle background process created by the bot """

from multiprocessing import active_children
from telegram_utils.model.cprocess import CProcess as Process
from telegram_utils.utils.misc import environment
import telegram_utils.utils.logger as logger

PROCESS_NAME = environment("PROCESS_PREFIX")


def find_process(name_prefix: str) -> Process:
    """ retrieve a process using a prefix name as a key """
    for process in active_children():
        # create the process name used when stored
        name: str = f"{PROCESS_NAME}_{name_prefix}"
        # if the name matches the created one
        if name == process.name:
            # return the process
            return process
    # return None if no process were found
    return None


def stop_process(key: str) -> None:
    """ Stop a background process identified by a key """
    # convert the key to a string to ensure it's working
    key = str(key)
    logger.info(f"restarting process with {key}")
    # find a process with that key
    process: Process = find_process(key)
    # if the process was not found exit and log a warn
    if not process:
        logger.warn(f"Unable to find the process with name {PROCESS_NAME % key}")
        return
    # terminate and shutdown the process
    process.terminate()
    process.shutdown()


def restart_process(key: str) -> None:
    """ Restart a background process identified by a key """
    # convert the key to a string to ensure it's working
    key = str(key)
    logger.info(f"restarting process with {key}")
    # find a process with that key
    process: Process = find_process(key)
    # if the process was not found exit and log a warn
    if not process:
        logger.warn(f"Unable to find the process with name {PROCESS_NAME % key}")
        return
    # shutdwon the current pprocess and create a new one
    target = process.target
    args = process.args
    process.terminate()
    process.shutdown()
    create_process(key, target, args)


def create_process(name_prefix: str, target: callable, args: tuple) -> None:
    """ Create a new background process and start it """
    # format the name of the process to be stored
    name: str = f"{PROCESS_NAME}_{name_prefix}"
    # create a process instance
    process: Process = Process(group=None, target=target, args=args, name=name)
    process.daemon = True
    # start the process
    logger.info(f"starting process {name} with {args}")
    process.start()
