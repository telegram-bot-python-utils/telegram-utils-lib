#!/usr/bin/env python3

""" various utilities """

from os import environ
from os.path import split
import sys
from xml.sax.saxutils import escape
import telegram_utils.utils.logger as logger


def environment(key: str) -> str:
    """ return the value of an environment variable if found """
    try:
        return environ[key]
    except KeyError:
        return None


def is_develop() -> bool:
    """Chek if the profile is set to develop

    Returns:
        bool: If the profile is set to develop
    """
    return environment["PROFILE"] == "develop"


def escape_value(value: object):
    """ convert any object to a str and escape it """
    return escape(str(value))


def format_error(error: Exception, bot_name: str = ""):
    """ format an exception showing the type, filename, line number and message """
    # extreact the information about the exception
    exc_type, _, exc_tb = sys.exc_info()
    # extreact the filename where the exception was raised
    logger.error(sys.exc_info())
    logger.error(exc_tb.tb_frame.f_code.co_filename)
    file_name = split(exc_tb.tb_frame.f_code.co_filename)[1]
    # return a formatted message with the exception info
    bot_name = f"<b>Bot: {bot_name}</b>\n" if bot_name else ""
    return (
        f"{bot_name}"
        f"<b>Exception Type:</b>  <code>{escape_value(exc_type)}</code>\n"
        f"<b>File Name:</b>  <code>{escape_value(file_name)}:{escape_value(exc_tb.tb_lineno)}</code>\n"
        f"<b>Message:</b>  <code>{escape_value(error)}</code>"
    )
