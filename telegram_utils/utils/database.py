#!/usr/bin/env python3

""" database utilitiles  """

import logging
from pymongo.mongo_client import MongoClient
from pymongo.errors import ServerSelectionTimeoutError, OperationFailure
from mongoengine import connect
from telegram_utils.utils.misc import environment
from telegram_utils.constants.messages import DATABASE_CONNECTION_ERROR
from telegram_utils.utils.tutils import log

USERNAME: str = "DBUSERNAME"
PASSWORD: str = "DBPASSWORD"
HOST: str = "DBHOST"
DATABASE: str = "DBNAME"
CONNECTION: str = "mongodb://{}:{}@{}/{}"


def db_connect():
    """ utility used to connect to a mongodb database using mongengine """
    # extract all the environment variables needed to connect
    username: str = environment(USERNAME)
    password: str = environment(PASSWORD)
    host: str = environment(HOST)
    database: str = environment(DATABASE)
    # format the connection string
    connection: str = CONNECTION.format(username, password, host, database)
    try:
        # connect to the database
        client: MongoClient = connect(host=connection)
        # retrieve server info, this is used to check if the connection went succesfully
        client.server_info()
        connect_translation()
    except (ServerSelectionTimeoutError, OperationFailure):
        # log an error message to file and Telegram
        log(logging.ERROR, DATABASE_CONNECTION_ERROR)


def connect_translation():
    try:
        # connect to the database
        client: MongoClient = connect("translation", alias="translation")
        # retrieve server info, this is used to check if the connection went succesfully
        client.server_info()
    except (ServerSelectionTimeoutError, OperationFailure):
        # log an error message to file and Telegram
        log(logging.ERROR, DATABASE_CONNECTION_ERROR)