#!/usr/bin/env python3

""" telegram function utils """

from time import sleep
from telegram.error import BadRequest
from telegram import InlineKeyboardButton
from telegram import Bot, Message
from telegram_utils.utils.misc import environment
from telegram_utils.utils.process import create_process
import telegram_utils.utils.logger as logger

ADMIN = environment("TELEGRAM_BOT_ADMIN")
TOKEN = environment("TOKEN")
LOG_CHANNEL = environment("LOG_CHANNEL")


def create_button(text: str, callback: str, url: str = None) -> InlineKeyboardButton:
    """ create a telegram compatible button """
    return InlineKeyboardButton(text, callback_data=callback, url=url)


def not_allowed_in_private(message: Message):
    """ send a message telling the user that the function works only on the group """
    send_and_delete(message.chat.id, "This function works only on groups", timeout=30)


def notice_admin(message: str):
    """ send a message to the admin of the bot """
    send_message(chat_id=ADMIN, message=message)


def send_message(
    chat_id: int,
    message: str,
    reply_markup=None,
    reply_to: int = None,
    disable_notification: bool = False,
) -> Message:
    """ send a message to a chat with keyboard or a quote """
    # create a bot instance
    bot: Bot = Bot(TOKEN)
    # send the message
    return bot.send_message(
        chat_id=chat_id,
        text=message,
        reply_markup=reply_markup,
        reply_to_message_id=reply_to,
        disable_notification=disable_notification,
        disable_web_page_preview=True,
        parse_mode="HTML",
    )


def send_and_delete(
    chat_id: int,
    message: str,
    reply_markup=None,
    reply_to: int = None,
    disable_notification: bool = False,
    timeout: int = 360,
):
    """ send a message and create a process to delete it automatically after a timeout """
    # fmt: off
    # send a message to Telegram
    message: Message = send_message(chat_id, message, reply_markup, reply_to, disable_notification)
    # fmt: on
    # retrive the message_id of that message
    message_id: int = message.message_id
    # create a process to automatically delete it after a timeout
    create_process(message_id, delete_message, (chat_id, message_id, timeout))


def delete_message(chat_id: int, message_id: int, timeout: int = 0):
    """ wait for the timeout and then proceed to delete the message (used by send_and_delete) """
    try:
        # create a bot instance
        bot: Bot = Bot(TOKEN)
        # wait for the timeout to end
        sleep(timeout)
        # delete the message
        bot.delete_message(chat_id=chat_id, message_id=message_id)
    except BadRequest:
        # ignore delete message errors, the message might be already deleted by an admin
        pass


def delete_if_private(message: Message):
    """ if the message comes from a private chat delete it """
    if message.chat.type == "private":
        delete_message(message.chat.id, message.message_id)


def log(level: int, message: str, disable_notification: bool = False):
    """ log to the file and to the log channel """
    logger.log(level, message)
    send_message(LOG_CHANNEL, message, disable_notification=disable_notification)