#!/usr/bin/env python3

""" Some constant string """

from telegram_utils.utils.misc import environment


DATABASE_CONNECTION_ERROR: str = (
    "An error occured while trying to connect to the database..."
)

TRANSLATION_PREFIX = environment("TRANSLATION_PREFIX")

TRANSLATION_TABLE_NAME = environment("TRANSLATION_TABLE_NAME")

TRANSLATION_TABLE_NAME_MISC = environment("TRANSLATION_TABLE_NAME_MISC")
