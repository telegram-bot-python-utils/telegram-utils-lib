import setuptools

long_description = "Library with some utilities for the various bot"

setuptools.setup(
    name="telegram-utils-lib",
    version="0.0.1",
    author="Edoardo Zerbo",
    author_email="edoardo.zerbo@gmail.com",
    description="Telegram Utils Lib",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
